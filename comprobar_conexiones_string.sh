#!/bin/bash

V_HOSTNAME=$(hostname)
V_HOST_LIST=$(cat /icas/current_build_slot/STRING_DEFINITION | egrep -v ":" | awk '{print $2}')
for V_HOST_SOURCE in $V_HOST_LIST
do
  V_HOST_SOURCE=`echo $V_HOST_SOURCE | sed -E "s/[^a-zA-Z0-9]*([a-zA-Z0-9]+)[^a-zA-Z0-9]*/\1/"`
  V_HOST_FOUND=$(timeout --foreground 3 ssh -t -l nor $V_HOST_SOURCE "hostname" 2> /dev/null)
  V_HOST_FOUND=`echo $V_HOST_FOUND | sed -E "s/[^a-zA-Z0-9]*([a-zA-Z0-9]+)[^a-zA-Z0-9]*/\1/"`
  if [[ "$V_HOST_FOUND" != "$V_HOST_SOURCE" ]] ; then
    echo -e "\n$V_HOSTNAME => $V_HOST_SOURCE"
  fi
  V_HOST_FOUND=$(timeout --foreground 5 ssh -t -l nor $V_HOST_SOURCE "ssh -t -l nor $V_HOSTNAME \"hostname\" 2> /dev/null" 2> /dev/null)
  V_HOST_FOUND=`echo $V_HOST_FOUND | sed -E "s/[^a-zA-Z0-9]*([a-zA-Z0-9]+)[^a-zA-Z0-9]*/\1/"`
  if [[ "$V_HOST_FOUND" != "$V_HOSTNAME" ]] ; then
    echo -e "\n$V_HOST_SOURCE => $V_HOSTNAME"
  fi
done