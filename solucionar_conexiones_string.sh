#!/bin/bash



function do_with_expect
{
expect << EXPECT_LINES
set timeout 200
spawn $@
expect {
-re "nor.*password" {
   send "goteras\r";
   exp_continue
}
-re "Are you sure you want to continue connecting" {
   send "yes\r";
   exp_continue
}
}
EXPECT_LINES
}

V_HOSTNAME=$(hostname)
V_HOST_LIST=$(cat /icas/current_build_slot/STRING_DEFINITION | egrep -v ":" | awk '{print $2}')
for V_HOST_SOURCE in $V_HOST_LIST
do
  do_with_expect "ssh-copy-id $V_HOST_SOURCE"
  do_with_expect "ssh -t -l nor $V_HOST_SOURCE \"ssh-copy-id $V_HOSTNAME\""
done